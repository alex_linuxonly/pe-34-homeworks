"use strict";
//TASK1
// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];


// let [...arr1] = clients1;

// let [...arr2] = clients2;
// console.log(arr1)
// console.log(arr2)


// if(arr1.length > arr2.length){
//   arr2.forEach(element => {
//     if(!arr1.includes(element)){
//       arr1.push(element)
//     }
//   });
// }
// else if(arr2.length > arr1.length){
// console.log ("ok");
// }

// console.log(arr1)


//TASK2


// const characters = [
//   {
//     name: "Елена",
//     lastName: "Гилберт",
//     age: 17, 
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Кэролайн",
//     lastName: "Форбс",
//     age: 17,
//     gender: "woman",
//     status: "human"
//   },
//   {
//     name: "Аларик",
//     lastName: "Зальцман",
//     age: 31,
//     gender: "man",
//     status: "human"
//   },
//   {
//     name: "Дэймон",
//     lastName: "Сальваторе",
//     age: 156,
//     gender: "man",
//     status: "vampire"
//   },
//   {
//     name: "Ребекка",
//     lastName: "Майклсон",
//     age: 1089,
//     gender: "woman",
//     status: "vempire"
//   },
//   {
//     name: "Клаус",
//     lastName: "Майклсон",
//     age: 1093,
//     gender: "man",
//     status: "vampire"
//   }
// ];

// let charactersShortInfo = [];

// characters.forEach(element => {
//     let {name, lastName, age, ...rest} = element;
//     let newObj = {name, lastName, age}
//     charactersShortInfo.push(newObj)
// });

// console.log(charactersShortInfo)

// //TASK3

// const user1 = {
//   name: "John",
//   years: 30
// };
// const {name: name, years: age, prop:isAdmin = false} = user1
// console.log(name)
// console.log(age)
// console.log(isAdmin)

//TASK 4 

// const satoshi2020 = {
//   name: 'Nick',
//   surname: 'Sabo',
//   age: 51,
//   country: 'Japan',
//   birth: '1979-08-21',
//   location: {
//     lat: 38.869422, 
//     lng: 139.876632
//   }
// }

// const satoshi2019 = {
//   name: 'Dorian',
//   surname: 'Nakamoto',
//   age: 44,
//   hidden: true,
//   country: 'USA',
//   wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//   browser: 'Chrome'
// }

// const satoshi2018 = {
//   name: 'Satoshi',
//   surname: 'Nakamoto', 
//   technology: 'Bitcoin',
//   country: 'Japan',
//   browser: 'Tor',
//   birth: '1975-04-05'
// }

// let fullProfile = {}



// const {...sat2018} = satoshi2018;
// const {...sat2019} = satoshi2019;
// const {...sat2020} = satoshi2020;


// let arr2018 = Object.getOwnPropertyNames(sat2018);

// let arr2019 = Object.getOwnPropertyNames(sat2019);

// let arr2020 = Object.getOwnPropertyNames(sat2020);

// arr2018.forEach(element => {
//   Object.defineProperty(fullProfile, element, {
//     value: sat2018[element],
//     writable:true
//   });
// });

// arr2019.forEach(element => {
//   Object.defineProperty(fullProfile, element, {
//     value: sat2019[element],
//     writable:true
//   });
// });

// arr2020.forEach(element => {
//   Object.defineProperty(fullProfile, element, {
//     value: sat2020[element],
//     writable:true
//   });
// });

// console.log(fullProfile)

// //TASK 5
// const books = [
//     {
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   },

//   {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   },

//   {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }
// ];
  
//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }



// let [one, two, three] = books

// let newArr = []

// newArr.push(one, two, three)

//  newArr.push(bookToAdd)
// console.log(newArr)

//TASK 6 
// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko',
//   };
  
//   const {name: Name, surname: Secondname} = employee

//  let newemployee={
//      name:Name,
//      surname:Secondname,
//      age:18,
//      salary:20,
//  };

// console.log(newemployee)


//TASK 7


const array = ['value', () => 'showValue'];

const[value, showValue] = array;

alert(value);

alert(showValue());