class Employee {
    constructor(name, age, salary){
       this.name = name;
       this.age = age;
       this.salary = salary;
       
    }
    set name(name){
        this._name = name;
    }
    get name(){
        return this._name;
    }
    set salary(salary){
        this._salary = salary;
    }
    get salary(){
        return this._salary
    }
    set age(age){
        this._age = age;
    }
    get age(){
        return this._age;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name,age,salary)
        this.lang = lang.join(', ');
    }
    set salary(salary){
        this._salary = salary * 3;
    }
    get salary(){
        return _salary;
    }
}


const sanya = new Programmer('Sanya', 25, 3000, ['eng', 'rus']);

const vitya = new Programmer('Vitya', 22, 1000, ['eng', 'rus']);

const dima = new Programmer('Dimon', 26, 1500, ['fr', 'uk']);

console.log(sanya)
console.log(vitya)
console.log(dima)