"use strict";
const books = [{
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];


class Element {
  constructor() {

  }
  render(content) {
    let li = document.createElement('li')
    li.textContent = content;
    return li;
  }
}


class CreateUl {
  constructor(author, name, price) {
    this.author = author;
    this.name = name;
    this.price = price;

  }
  render(element, index) {
    let error = 0;
    let templateObj = {
      author: "",
      name: "",
      price: "",
    }
    let templateArr = Object.getOwnPropertyNames(templateObj);
    let propArr = Object.getOwnPropertyNames(element)
    
    templateArr.forEach((value) => {
      if(!(propArr.includes(value))){
        console.log(` Object number ${index+1} is missing ${value} property`)
        error = error+ 1;
      }
      
    });
   
   if(error === 0){
    let elem = new Element().render(this.author)
    let elem2 = new Element().render(this.name)
    let elem3 = new Element().render(this.price)
    let ul = document.createElement('ul')
    ul.append(elem)
    ul.append(elem2)
    ul.append(elem3)
    
    return (ul)
   }else{
     return('')
   }
    

  }

}



class List {
  constructor(books) {
    this.books = books;
  }
  render(root = document.querySelector('#root')) {
    this.books.forEach((element, index) => {
   
        let ul = new CreateUl(element.author, element.name, element.price).render(element, index);
        root.append(ul)
  



    });
  }
}

const test = new List(books);

test.render()