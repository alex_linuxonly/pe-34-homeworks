"use strict";
const url = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.getElementById('root');

const spinner = document.querySelector('.spinner')

class Server {
    constructor(url){
        this.url =url;

    }
    getRequest(url){
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.send();
            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(e);
        });

    }
    createElement(tag){
        let elem = document.createElement(tag)
        return elem
    }
    
    getData(){
        this.getRequest(this.url)
        .then(data => {
           let responseArr = JSON.parse(data)
           console.log(responseArr)


            responseArr.forEach(({name, episodeId, openingCrawl, characters}) => {
                
                    const elemName = this.createElement('h3')
                    elemName.textContent = name
              
               
                    const elemEpisodeId = this.createElement('h4')
                    elemEpisodeId.textContent = `Episode number ${episodeId}`
                    
             
                
                    const elemOpeningCrawl = this.createElement('p')
                    elemOpeningCrawl.textContent = openingCrawl
                   
                    const cont = this.createElement('div')
                    cont.classList.add('container')
                    const spinMain = this.createElement('div')
                    spinMain.classList.add('spinner')
                    const spinItem1 = this.createElement('div')
                    spinItem1.classList.add('spinner-item')
                    const spinItem2 = this.createElement('div')
                    spinItem2.classList.add('spinner-item')
                    const spinItem3 = this.createElement('div')
                    spinItem3.classList.add('spinner-item')
                    const spinItem4 = this.createElement('div')
                    spinItem4.classList.add('spinner-item')
                    const spinItem5 = this.createElement('div')
                    spinItem5.classList.add('spinner-item')
                    
                    spinMain.append(spinItem1, spinItem2, spinItem3, spinItem4, spinItem5)
                    cont.append(spinMain)
                    let ul = this.createElement('ul');

                    const charList = [];
                        characters.forEach(characterURL =>{
                            charList.push(this.getRequest(characterURL))
                           
                    })
                   
                    const character = Promise.all(charList)
                    character.then((data)=>{
                        data.forEach(el =>{
                            console.log(el);
                            let li = this.createElement('li')
                            el = JSON.parse(el).name
                            li.textContent=el
                            spinMain.classList.add('noactive')
                            ul.append(li)
                        })
                    })
           
                
                root.append(elemName, elemEpisodeId, elemOpeningCrawl, cont, ul )
               
                
              
            });
        })
    }
}
let film = new Server(url);
film.getData()
