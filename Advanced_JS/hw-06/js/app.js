const url_ip = 'https://api.ipify.org/?format=json'
const url_add = 'http://ip-api.com/json'

const root = document.getElementById('root');
class Btn {
    constructor(name){
    this.name = name
    }
    
    render(arr){
        let fragment = document.createDocumentFragment()
        console.log(arr)
             arr.forEach(element => {
              let li = document.createElement('li')
              li.textContent = element;
              fragment.append(li)
           });
           console.log(fragment)
    
        root.append(fragment)
        
    }
    getData(){
        let btn = document.createElement('button');
        btn.textContent = this.name
        root.append(btn)
        btn.addEventListener('click', async()=>{
            let ip = await axios.get(url_ip)
            ip = ip.data.ip
            let info = await axios.get(url_add+'/'+ ip)
            info = info.data
        
            
            let arrInfo = [info.continent, info.region, info.country, info.regionName,  info.city, info.district]
            this.render(arrInfo)
        })
    }

}

const button = new Btn('Get ip')

button.getData()