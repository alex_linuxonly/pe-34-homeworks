import styles from "./Header.module.scss"
import { NavLink } from "react-router-dom";


function Header () {

    return(
        <div className={styles.shop_header}>
         <ul className = {styles.shop_menu}>
             
             <li>
                  <NavLink exact to="/cart" className = {styles.shop_menu_link} activeClassName={styles.shop_menu_link_active}> Cart </NavLink>
            </li>

             <li> 
                 <NavLink exact to="/" className={styles.shop_title} activeClassName={styles.shop_menu_link_active}>West Coast Cars</NavLink> 
            </li>
            
             <li> 
                 <NavLink exact to="/favourites" href="" className = {styles.shop_menu_link} activeClassName={styles.shop_menu_link_active}> Favourite</NavLink>
             
             </li>

        </ul>   
        


        </div>
    )



}

export default Header