import React from "react";
import PropTypes from 'prop-types'
import './Modal.scss'
import { useDispatch } from "react-redux";
import { addToCart, deleteFromCart } from "../../store/actionsCreator/actionsCreator";

function Modal(props)  { 
    const {header, shouldClose, func, currentItem, modalType} = props
    console.log(currentItem)
    const dispatch = useDispatch()
    
    let method;
    let mainText;
    
    if(modalType === 'addItem'){
        method = addToCart
        mainText = 'Are you sure you want to add this item to the cart?'
    }else{
        method = deleteFromCart
        mainText = 'Are you sure you want to delete this item from the cart?'
    }
    console.log(method)

    return(
        <div className = {"modal active"}  onClick={func}>
            <div className={"modalContent"}>
            <div className={"modalContentHeader"} >
            <header className={'modalHeader'}> {header} </header>
                {shouldClose}
                </div>
            
            
            <p className={'mainText'}>{mainText}</p>
            <div className={'modalButtons'}>
             
            <button onClick={() => {dispatch(method(currentItem))}} style={{backgroundColor:'grey', width:'70px', height:'25px'}} >OK</button>
            <button style={{backgroundColor:'grey', width:'70px', height:'25px'}} >Cancel</button>
            </div>
           
            </div>
        </div>
    )

    
}


Modal.propTypes = {
    header: PropTypes.string,
    shouldClose: PropTypes.object,
    func: PropTypes.func.isRequired,
    mainText: PropTypes.string,
    addItem : PropTypes.func.isRequired,
    currentItem: PropTypes.string.isRequired, 
}

export default Modal;