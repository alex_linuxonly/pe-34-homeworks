import React from 'react';
import styles from './ItemsContainer.module.scss';
import Item from "../Item";
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux';
import { addToCart } from '../../store/actionsCreator/actionsCreator';

function ItemsContainer (props) {
    
   
        const {cars, openModal, typeCard} = props;

        const dispatch = useDispatch();


        if(typeCard === 'mainPage'){
            return (
                <section className={styles.root}>
                    <h1 className={styles.titleText}>CARS</h1>
                    <div className={styles.container}>
                        {cars && cars.map(cars =>  <Item key={cars.id} {...cars} addItem={dispatch(addToCart)} openModal={openModal}  />)}
                    </div>
                </section>
            )
        }else if(typeCard === 'favPage'){
            console.log(typeCard)
            return (
                <section className={styles.root}>
                    <h1 className={styles.titleText}>CARS</h1>
                    <div className={styles.container}>
                        {cars && cars.map((cars) =>  {
                            if(cars.isFavorite === true){
                            return <Item key={cars.id} {...cars} addItem={dispatch(addToCart)} openModal={openModal}  />
                        }

                         })}
                    </div>
                </section>
            )
        }            


        
    
    
}

ItemsContainer.propTypes = {
    items: PropTypes.array,
    addItem : PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    toggleFav : PropTypes.func.isRequired,

}


export default ItemsContainer;