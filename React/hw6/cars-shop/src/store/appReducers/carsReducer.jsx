import { GET_ITEMS, TOGGLE_FAV } from "../actions/actions";

const initialState = {
    cars:[],
};

export const carsReducer = (state = initialState, action) => {
    if(action.type === GET_ITEMS){
        console.log(action.payload)
        
        return {...state, cars:[...action.payload]}
    
    }
    else if(action.type === TOGGLE_FAV){
        
        const index = state.cars.findIndex(({ Name }) => {
            return Name === action.payload;
          })
            const newArrcars = [...state.cars];
           if(newArrcars[index].isFavorite === false){
            newArrcars[index].isFavorite = true
            return {...state, cars:[...newArrcars]}
           }else{
            newArrcars[index].isFavorite = false
            return {...state, cars:[...newArrcars]}
           }
    }
    else{
        return state
    }
 
}

