import { ADD_TO_CART, DELETE_FROM_CART, GET_DATA_TO_CART } from "../actions/actions"

const initialState = {
    cartItems:[],
}


export const cartReducer = (state = initialState, action)=>{
    
    if(action.type === ADD_TO_CART)
    {
        console.log(action.payload)
        
        const index = state.cartItems.findIndex(({Name})=>{
            return action.payload === Name
        })
        console.log(index)
        if(index === -1)
            {
                return {...state, cartItems:[...state.cartItems, {Name:action.payload.Name, count: 1, Price:action.payload.Price,}]}
            }
        else
            {
                const newState = [...state.cartItems]
                console.log(newState)
                newState[index].count = newState[index].count+1
                return {...state, cartItems:[...newState]}
            }
        

    }
    else if(action.type === DELETE_FROM_CART)
    {
        const index = state.cartItems.findIndex(({Name})=>{
            return action.payload === Name
        })
        const newState = [...state.cartItems];
          if(newState[index].count === 1){
            newState.splice(index, 1)
          }
          else{
            newState[index].count = newState[index].count -1;
          }
          
            return {...state, cartItems:[...newState]}
       
    }
    else if(action.type === GET_DATA_TO_CART)
        {
            return{...state, cartItems:[...action.payload]}
        }
    else
        {
            return state
        }
}