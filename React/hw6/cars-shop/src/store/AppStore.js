import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import { appReducer } from "./appReducers/appReducer";


export const store = createStore(appReducer, composeWithDevTools(applyMiddleware(thunk)));