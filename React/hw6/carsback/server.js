const express = require('express');
const path  = require('path');
const morgan = require('morgan');
const mongoose = require("mongoose");
const Post = require('./models/post')
const Contact = require('./models/contact')

const app = express();

app.set('view engine', 'ejs');

const PORT = 3000;
const db = 'mongodb+srv://oleksii:JRRdlkIb6C3E3Hkh@cluster0.empyk.mongodb.net/?retryWrites=true&w=majority'

mongoose
.connect(db, { useNewUrlParser: true, useUnifiedTopology: true})
.then((res) => console.log('Connected to DB'))
.catch(err => console.log(err))

const createPath = (page) => path.resolve(__dirname, 'ejs-views', `${page}.ejs`)

app.listen(PORT, (error) => {
    error ? console.log(error) : console.log('listening port 3000')
});

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use(express.urlencoded({extended: false }));

app.use(express.static('styles'));



app.get('/', (req, res) => {
    const title = 'Home';
    res.render(createPath('index'), {title});
})

app.get('/contacts', (req, res) => {
    const title = 'Contacts';
    Contact
    .find()
    .then((contacts) => res.render(createPath('contacts'), {contacts, title}))
    .catch((error) => { 
        console.log(error);
        res.render(createPath('error'), {title: 'Error'})
    });
});

app.get('/posts/:id', (req, res) => {
    const title = 'Post';
    const post = {
        id: '1',
        text: "Lorem sadsdfasdfasdfasdfasdfasdfasdf ads fasd fasdf sd fa dsf asdf asdf adsf asd fa dsf ",
        title: "Super title",
        date: "05.05.2021",
        author: 'Yauhen',

    };
    res.render(createPath('post'), {title, post });
})

app.get('/posts', (req, res) => {
    const title = 'Posts';
    Post
    .find()
    .then((posts) => res.render(createPath('posts'), {posts, title}))
    .catch((error) => { 
        console.log(error);
        res.render(createPath('error'), {title: 'Error'})
    });
})

app.post('/add-post', (req, res)=>{
    const { title, author, text} = req.body;
    const post = new Post ({ title, author, text});
    post
    .save()
    .then((result) => {res.send(result); res.render(createPath('index'))})
    .catch((error) => { 
        console.log(error);
    })
    // const post = {
    //     id: new Date(),
    //     date : (new Date()).toLocaleDateString(),
    //     title, 
    //     author,
    //     text
    // }
    // res.render(createPath('posts'), {post, title})
});

app.get('/add-post', (req, res) => {
    const title = 'Add-post';
    res.render(createPath('add-post'), {title});
})

app.use((req, res) => {
    const title = 'Error-page';
    res
    .status(404)
    .render(createPath('error'), {title});
})

