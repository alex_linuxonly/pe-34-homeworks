import React from "react";
import {Switch, Route} from 'react-router-dom' 
import CartPage from '../pages/CartPage/CartPage'
import MainPage from "../pages/MainPage/MainPage";
import FavPage from "../pages/FavPage/FavPage";

function Routes (props){
   
    const {cars, openModal, cartItems, toggleFav, addItem, isModalOpened, closeModal} = props
    
    return(
        <Switch>
            <Route exact path='/cart'>
                <CartPage cartItems={cartItems}  openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} component={CartPage} />
            </Route>
            <Route exact path='/'>
                <MainPage  cars={cars} modalType='okay' toggleFav={toggleFav} addItem={addItem} openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
            </Route>
            <Route exact path='/favourites'>
                <FavPage cars={cars} toggleFav={toggleFav} addItem={addItem} openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
            </Route>

        </Switch>
    )
}
export default Routes;