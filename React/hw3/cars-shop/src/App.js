import React, { useState, useEffect } from 'react';
import './App.scss';
import Modal from './components/modal/Modal';
import background from './assets/imgs/Background02.jpg'
import Header from './components/Header/Header';
import Routes from './Routes/Routes';


function App () {

  const [cars, setCars] = useState([])
  const [cartItems, setCartItems] = useState([])
  const [isModalOpened, setIsModalOpened] = useState(false)
  const [closeBtn, setCloseBtn] = useState(true)
  const [currentItem, setCurrentItem] = useState('')
  const [modalType, setModalType] = useState('')

  useEffect(async() => {
    
      const arrayCart = JSON.parse(localStorage.getItem('cartItems'))
      console.log(arrayCart)
  
      let arrayCars = JSON.parse(localStorage.getItem('cars'))
      console.log(arrayCars)
      if(!arrayCars || arrayCars.length<1) {
        const response = await fetch('cars.json')
        .then((e) => e.json())
        setCars(response)
        console.log(cars)
  
      }else{
        setCars(arrayCars)
        console.log(cars)
      }
      
      if(arrayCart){
        
        setCartItems(arrayCart)
      }
    
  }, [])
    

useEffect(() => {
  console.log('2')
      localStorage.setItem('cartItems', JSON.stringify(cartItems))
      
}, [cartItems])
useEffect(() => {
  console.log('3')

      localStorage.setItem('cars', JSON.stringify(cars))
}, [cars])
  
    
      
  

 const addItem = (Name) => {
    const index = cartItems.findIndex(({ Name: arrayName }) => {
      return Name === arrayName;
    })
    if (index === -1) {
      setCartItems((current) => [...current, {Name, count: 1}])
        
    } else {
      const newState = [...cartItems];
      newState[index].count = newState[index].count +1;
      setCartItems((current) => {
        return newState;
    })
      
    }
    localStorage.setItem('cartItems', JSON.stringify(cartItems))
    
  }

  const deleteItem = (Name) => {
    const index = cartItems.findIndex(({ Name: arrayName }) => {
      return Name === arrayName;
    })
    
      const newState = [...cartItems];
      if(newState[index].count === 1){
        newState.splice(index, 1)
      }
      else{
        newState[index].count = newState[index].count -1;
      }
      setCartItems((current) => {
        return newState;
    })
    localStorage.setItem('cartItems', JSON.stringify(cartItems))
    
    }
    
  

 const openModal = (name, type='addtocart') => {
   if(type==='addtocart'){
     console.log('ADD???')
     setCurrentItem(name)
    setIsModalOpened(true)
    setModalType("addItem")
   }else{
    setCurrentItem(name)
    setIsModalOpened(true)
    setModalType("deleteItem")
     console.log('close')
   }
   

  }

 const closeModal = () => {
   setIsModalOpened(false)
  
  }

const toggleFav = (Name)=> {
  const index = cars.findIndex(({ Name: arrayName }) => {
    return Name === arrayName;
  })
  console.log(index)

    const newArrcars = [...cars];
   if(newArrcars[index].isFavorite === false){
    newArrcars[index].isFavorite = true
    setCars(()=>{
      return newArrcars
    })
    
   }else{
    newArrcars[index].isFavorite = false
    setCars(()=>{
      return newArrcars
    })
    
   }
    
  
}
  
    

    return (
      
      <div style={{ backgroundImage: `url(${background})`, paddingBottom:'auto' }}>
        
        <Header />
       <Routes cars={cars} toggleFav={toggleFav} addItem={addItem} openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} cartItems={cartItems} />
      <div className="root" >
        {isModalOpened ? <Modal addItem={addItem} deleteItem={deleteItem} modalType={modalType} currentItem={currentItem} header={'Attention'}  items={cars} shouldClose={closeBtn ? <span className={"close"} onClick={closeModal}>&times;</span> : ''} func={closeModal} /> : ''}
        {/* <div>
          <ItemsContainer items={cars} toggleFav={toggleFav} addItem={addItem} openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
        </div> */}
        {/* <div>
          <Cart items={cartItems} />
        </div> */}
      </div>
      </div>
     
    );
  


}

export default App;
