import React from 'react';
import styles from './Cart.module.scss';
import CartItem from "../CartItem";
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux';

const Cart = (props) => {
    const {  id, openModal } = props;
    const cartItems  = useSelector(state =>{
        return state.cart.cartItems
    })
    console.log(cartItems)
    return (
        <section className={styles.root}>
            {/* <h2 className={styles.cart_header} > CART </h2> */}
            <div className={styles.items}>
                {cartItems && cartItems.map(item => <CartItem key={id} openModal={openModal} {...item} />)}
            </div>
            <p className={styles.textColorPrice}  >Price: 0$</p>
        </section>
    )
}

Cart.propTypes = {
    items: PropTypes.array,
    id: PropTypes.number.isRequired,

}
export default Cart;