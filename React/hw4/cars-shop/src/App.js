import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './App.scss';
import Modal from './components/modal/Modal';
import background from './assets/imgs/Background02.jpg'
import Header from './components/Header/Header';
import Routes from './Routes/Routes';
import { getData, setCars, setIsOpen, getDataToCart } from './store/actionsCreator/actionsCreator';

function App () {

  const cars = useSelector((state)=> {
    console.log(state)
    return state.items.cars
  })

  const cartItems = useSelector((state)=> {
    return state.cart.cartItems
  })

  const isModalOpened = useSelector((state)=> state.modal.isOpen )
  
  const dispatch = useDispatch()




  const [closeBtn, setCloseBtn] = useState(true)
  const [currentItem, setCurrentItem] = useState('')
  const [modalType, setModalType] = useState('')


  
  useEffect(() => {
    let arrayCars = JSON.parse(localStorage.getItem('cars'))
    const arrayCart = JSON.parse(localStorage.getItem('cartItems'))
    if(!arrayCars || arrayCars.length<1) {
      console.log("1")
      dispatch(getData())
    }else{
      console.log("2")
      dispatch(setCars(arrayCars))
    }
    if(arrayCart){
            dispatch(getDataToCart(arrayCart))
          }
}, [])


useEffect(() => {
      localStorage.setItem('cartItems', JSON.stringify(cartItems))
}, [cartItems])
useEffect(() => {
      localStorage.setItem('cars', JSON.stringify(cars))
}, [cars])
  
    

 const openModal = (name, type='addtocart') => {
   if(type==='addtocart'){
     setCurrentItem(name)
    dispatch(setIsOpen(true))
    setModalType("addItem")
   }else{
    setCurrentItem(name)
    dispatch(setIsOpen(true))
    setModalType("deleteItem")
   }
  }

 const closeModal = () => {
   dispatch(setIsOpen(false))
  }
  
    return (
    
        <div style={{ backgroundImage: `url(${background})`, paddingBottom:'auto' }}> 
          <Header />
        <Routes cars={cars}  openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} cartItems={cartItems} />
        <div className="root" >
          {isModalOpened ? <Modal  modalType={modalType} currentItem={currentItem} header={'Attention'}  items={cars} shouldClose={closeBtn ? <span className={"close"} onClick={closeModal}>&times;</span> : ''} func={closeModal} /> : ''}
        </div>
        </div>
      
    );
  


}

export default App;
