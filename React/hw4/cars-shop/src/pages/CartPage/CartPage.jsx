import styles from './CartPage.scss'
import background from '../../assets/imgs/Background02.jpg'
import Cart from "../../components/Cart/Cart";
import { useSelector } from 'react-redux';

function CartPage (props) {
    const { openModal } = props
    const cartItems  = useSelector(state =>{
        return state.cart.cartItems
    })
    console.log(cartItems)

    return(
        <div className={styles.cartPage_main} style={{backgroundImage:background, height:"100vh"}}>
            <div>
                 <Cart openModal={openModal} cartItems={cartItems} />
            </div>
        </div>
    )
}
export default CartPage