import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

function MainPage({cars, openModal, isModalOpened, closeModal }){

    return(
        <div>
          <ItemsContainer typeCard='mainPage' cars={cars}  openModal={openModal} isModalOpened={isModalOpened} closeModal={closeModal} />
        </div>


    )

}
export default MainPage