import React from 'react';
import styles from './Item.module.scss';
import PropTypes from 'prop-types';
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg"
import { setIsOpen, toggleFav } from '../../store/actionsCreator/actionsCreator';
import { useDispatch } from 'react-redux';

function Item (props) { 
        
        const { Name, Price, url,  openModal, isFavorite } = props;
        const dispatch = useDispatch()

        return (
            <div className={styles.root}>
                <div className={styles.favourites}>
                   {isFavorite ? <StarAdd className={styles.star_fav}  onClick={()=>{dispatch(toggleFav(Name))}} /> :<StarAdd className={styles.star_notfav}  onClick={()=>{dispatch(toggleFav(Name))}} />}
                </div>
                <p className={styles.textColor} >{Name.toUpperCase()}</p>
                <img  src={url} alt={Name} />
                <span className={styles.card_price}>{Price}$</span>
                {/* <button className={styles.card_btn} onClick={()=>{openModal({Name, Price})}}>Add to cart</button> */}
                <button className={styles.card_btn} onClick={()=>{dispatch(setIsOpen({Name, Price}))}}>Add to cart</button>                
            </div>
        )
    
    
}

Item.propTypes = {
    Name: PropTypes.string.isRequired,
    Price: PropTypes.number.isRequired,
    url : PropTypes.string.isRequired,
    openModal: PropTypes.func.isRequired,
    isFavorite: PropTypes.bool,
    toggleFav : PropTypes.func.isRequired,

}


export default Item;