import React from "react";

import './Modal.scss'



class Modal extends React.Component { 

    

render (){
    const {header, shouldClose, mainText, func} = this.props

    return(
        
        <div className = {"modal active"}  onClick={func}>
            
            <div className={"modalContent"}>
            <div className={"modalContentHeader"} >
            <header className={'modalHeader'}> {header} </header>
                {shouldClose}
                </div>
            
            
            <p>{mainText}</p>
            <div>
            <button onClick={()=>{console.log('hello')}} style={{backgroundColor:'blue', width:'50px', height:'25px'}} >OK</button>
            <button style={{backgroundColor:'orange', width:'50px', height:'25px'}} >Cancel</button>
            </div>
           
            </div>
        </div>
    )
}
    
}
export default Modal;