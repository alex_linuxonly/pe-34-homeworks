import React from 'react';
import Modal from './components/modal/Modal';
import './App.css';

import Button1 from './components/Button1/Button1.jsx';



class App extends React.Component {
  state = {
    isFirstOpened: false,
    isSecondOpened: false,
    closeBtn: true,
   
  }

  showModalFirst = (event) => {
 
    if(event.target.innerText === 'First'){
      this.setState((current) =>({
        ...current,
        isFirstOpened:true,
      }))  
    }else{
      this.setState((current) =>({
        ...current,
        isSecondOpened:true,
      }))
     
      
    }
    
  }

  toggleClose = () =>{
    this.setState((current) =>({
      ...current,
      isFirstOpened:false,
      isSecondOpened:false,
    }))  
    
  }

  render() {
    const { isFirstOpened, isSecondOpened, closeBtn  } = this.state;
    
    return (
      <div class='App'>
        <Button1 number={'first'} text={"First"} bgcolor={"red"} onClick={this.showModalFirst} />
        <Button1 text={"Second"} bgcolor={"blue"} onClick={this.showModalFirst} />
        {isFirstOpened ? <Modal shouldClose={closeBtn ? <span  className={"close"} onClick={this.toggleClose}>&times;</span> : ''} func={this.toggleClose} header={'this is first modal window header'} mainText={'Main TEXT OF FIRTST MODAL WINDOWS'} /> : ''}
        {isSecondOpened ? <Modal shouldClose={closeBtn ? <span className={"close"} onClick={this.toggleClose} >&times;</span> : ''} func={this.toggleClose} header={'this is second modal window header'} mainText={'Main text of the SECOND MODAL WINDOWS'} /> : ''}
      </div>
    )
  }
}
export default App