import React from 'react';
import styles from './Cart.module.scss';
import CartItem from "../CartItem";
import PropTypes from 'prop-types'


class Cart extends React.Component  {
    
    
    render(){
        const { items, id } = this.props;
        return (
            <section className={styles.root}>
                <h2 className={styles.textColor} > CART </h2>
                <div className={styles.items}>
                    {items && items.map(item => <CartItem key={id} {...item} />)}
                </div>
                <p className={styles.textColor}  >Price: 0$</p>
            </section>
        )
    }
        
}
Cart.propTypes = {
    items: PropTypes.array,
    id: PropTypes.number.isRequired,

}
export default Cart;