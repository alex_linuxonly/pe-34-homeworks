document.addEventListener("DOMContentLoaded", function () {
    'use strict';
    let span = document.createElement('span');
    document.getElementById('input').addEventListener('focus', function () {
        this.classList.add('hasfocus')
    })

    document.getElementById('input').addEventListener('blur', function (event) {
        span.remove()

        this.classList.remove('hasfocus')
        let btn = document.createElement('button')
        btn.textContent = 'x'
        btn.setAttribute('id', 'btn')
        if (this.value < 0 || !this.value) {
            span.textContent = 'Please enter correct price'
            this.style.color = 'red'
            document.body.append(span)
            this.classList.add('wrongNumber')
        } else {
            this.classList.remove('wrongNumber')
            span.textContent = `The current price is ${this.value}`
            this.style.color = 'green'
            document.body.prepend(span)
            span.append(btn)


            document.getElementById('btn').addEventListener('click', function () {
                span.remove()
                event.target.style.color = 'black'
                event.target.value = ''
            })
        }
    })

})