document.addEventListener('DOMContentLoaded', function () {

    let eyebtn = document.getElementsByTagName('i')

    let input = document.getElementsByTagName('input')

    let btncheck = document.getElementById('btn1')

    let alleyebtns = Array.from(eyebtn)
        
    let allinputs = Array.from(input)

    function checkPasswd(event) {
        
        function checkEye(dataname){
            alleyebtns.forEach(element => {
                        
                if(element.classList.contains(dataname)&&element.classList.contains('noactive')){
                    element.classList.remove('noactive')
                    element.classList.add('active')
                }
               else if(element.classList.contains(dataname)&&element.classList.contains('active')){
                    element.classList.remove('active')
                    element.classList.add('noactive')
               }
            });
        }
        allinputs.forEach(element => {
            let dataname = element.getAttribute('data-input-name')
            if (event.target.classList.contains(dataname)) {
                if (element.getAttribute('type') === 'password') {
                    checkEye(dataname)
                    element.setAttribute('type', 'text')
                } 
                else{
                    element.setAttribute('type', 'password')
                    checkEye(dataname)
                }
                
            }

        })
        
    }

    let mass = Array.from(eyebtn)
    mass.forEach(element => {
        element.addEventListener('click', checkPasswd)
    });

    function comparePasswd(event){
        
        event.preventDefault()
        let input1;
        let input2;
        let span = document.getElementById('span')
        allinputs.forEach(element => {
            if(element.getAttribute('data-input-name')==='input1'){
                input1 = element
            }else{
                input2 = element
            }
        });
      if(input1.value !== input2.value){
        span.classList.remove(`noactive`)
        span.classList.add(`active`)
      }else{
        span.classList.add(`noactive`)
        span.classList.remove(`active`)
        input1.value=''
        input2.value=''
        alert('You are welcome!')
        
      }
    }

    btncheck.addEventListener('click',comparePasswd)
})