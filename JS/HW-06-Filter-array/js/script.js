'use strict';

function filterBy(array, type){
    let newArray = []
    array.forEach(element => {
        if (typeof(element)!==type){
            newArray.push(element)
        }
    });
    return newArray;
}
const func = filterBy(['hello', 'world', 23, '23', null], 'string')
console.log(func)