document.addEventListener('DOMContentLoaded', function(){
'use strict';
let ul = document.getElementById('idtabs')
let alLi = document.getElementsByClassName('tabs-title')

const test = (event) => {
    console.log(event.target.id);
    Array.from(alLi).forEach((element) => {
        element.classList.remove('active')
    })
    event.target.classList.add('active')
    let tab = document.getElementsByClassName('tab')
    for (let i = 0; i < tab.length; i++) {
        tab[i].classList.add('no-active')
        tab[i].classList.remove('active')
        if (tab[i].classList.contains(event.target.id)) {
            tab[i].classList.add('active')
        }
    }
    
}

ul.addEventListener('click', test)
})