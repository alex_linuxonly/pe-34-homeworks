$('document').ready(function () {
    $('.hero_navi_menu_link').click(function () {
        $('html, body').animate({
            scrollTop: $(this.getAttribute('href')).offset().top
        }, 2000)
    })


    var $win = $(window);
    var winH = $win.height(); // Get the window height.
    let key = 1

    $win.on("scroll", function () {
        if ($(this).scrollTop() > winH && key === 1) {
            $('#btn').show()
            
        }
    })

    $('#btn').click(function(){
        $('#btn').hide()
    })

    $('#close').click(function(){
        $('.top_rated').slideToggle()
    })
})