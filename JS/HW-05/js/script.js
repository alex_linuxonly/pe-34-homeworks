'use strict';

function createNewUser() {
    let firstname = prompt("Entrer name: ");
    let secondname = prompt("Enter secondname: ");
    let birthdate = prompt('Enter please birthdate in dd.mm.yyyy: ')
    let age = 0
    let useryear = 0
    let usermonth = 0
    let userday = 0
    const newUser = {
        firstName: firstname,
        lastName: secondname,
        birthday: birthdate,
        getLogin: function () {
            return ((this.firstName[0].toUpperCase() + this.lastName.toLowerCase()));
        },
        getAge: function () {
            let nowyear = (new Date().getFullYear());
            let nowmonth = (new Date().getMonth());
            let nowday = (new Date().getDate());
            useryear = this.birthday.substr(-4)
            userday = Number(this.birthday.slice(0, 2))
            usermonth = Number(this.birthday.slice(3, 5))
            nowmonth = nowmonth + 1
            
            age = nowyear - useryear

            if (nowmonth > usermonth) {
                return ((`User age is ${age}`))
            } else if (nowmonth === usermonth) {
                if (nowday > userday) {
                    return (`User age is ${age}`)
                } else if (nowday === userday) {
                    return (`User age is ${age}`)
                } else {
                    return (`User age is ${age-1}`)
                }
            } else {
                return (`User age is ${age-1}`)
            }

        },

        getPassword: function () {
            return (this.getLogin() + useryear)
        }
    }
    return newUser;
}

const func = createNewUser()
console.log(func)
// console.log(func.getLogin().toLowerCase())
console.log(func.getAge())
console.log(func.getPassword())