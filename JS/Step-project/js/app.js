document.addEventListener('DOMContentLoaded', function () {
    'use strict';

    let servul = document.getElementById("servicesidtabs")
    let alLi = document.getElementsByClassName('services_tabs_title')

    const changeTab = () => {
        Array.from(alLi).forEach((element) => {
            element.classList.remove('tab-active')
        })
        event.target.classList.add('tab-active')
        let tab = document.getElementsByClassName('servtab')
        for (let i = 0; i < tab.length; i++) {
            tab[i].classList.add('no-active_serv')
            tab[i].classList.remove('active_serv')
            if (tab[i].classList.contains(event.target.id)) {
                tab[i].classList.add('active_serv')
            }
        }

    }

    servul.addEventListener('click', changeTab)







    let loadbtn = document.getElementById('load_more_btn')
    console.log(loadbtn)
    const photosArray = ['./imgs/amazing_work/Layer 36.jpg',
        './imgs/amazing_work/Layer 37.jpg',
        './imgs/amazing_work/Layer 38.jpg',
        './imgs/amazing_work/Layer 39.jpg',
        './imgs/amazing_work/Layer 40.jpg',
        './imgs/amazing_work/Layer 41.jpg',
        './imgs/amazing_work/Layer 42.jpg',
        './imgs/amazing_work/Layer 43.jpg',
        './imgs/amazing_work/Layer 44.jpg',
        './imgs/amazing_work/Layer 45.jpg',
        './imgs/amazing_work/Layer 46.jpg',
        './imgs/amazing_work/Layer 47.jpg'
    ]
    const amazinglitabs = document.getElementsByClassName('amazing-tabs-title')
    const imgset = document.getElementById('amazing_photos_set')
    let li = document.getElementById('amazing_photo_item')
    let dataNameArray = ['graphic_design', 'web_design', 'landing_pages', 'wordpress']
    let defaulttarget = document.getElementById('amazing-tab1')
    let click = defaulttarget

    function loadMore() {

        for (let i = 0; i < photosArray.length; i++) {
            let cloneLi = li.cloneNode(true)
            cloneLi.removeAttribute('id')
            cloneLi.childNodes[1].setAttribute('class', 'amazing_photo')
            cloneLi.childNodes[1].setAttribute('src', photosArray[i])
            cloneLi.setAttribute('data-name', dataNameArray[Math.floor(Math.random() * dataNameArray.length)])
            imgset.append(cloneLi)
        }
        loadbtn.remove()
        checkPicType(click)
    }

    loadbtn.addEventListener('click', loadMore)




    const amazingtabs = document.getElementById('amazingidtabs')

    let allpics = document.getElementsByClassName('amazing_photo_item')

    function selectTab(event) {
        click = event.target

        Array.from(amazinglitabs).forEach(element => {
            element.classList.remove('amazing-tab-active')
        });
        event.target.classList.add('amazing-tab-active')

        checkPicType(event.target)

    }

    function checkPicType(target = defaulttarget) {
        if (target.getAttribute('data-name') === 'all') {

            Array.from(allpics).forEach(element => {
                element.classList.remove('noactive_amazing_item')
                element.classList.add('active_amazing_item')
            });
        } else {

            console.log(target.getAttribute('data-name'))
            Array.from(allpics).forEach(element => {
                element.classList.remove('active_amazing_item')
                element.classList.add('noactive_amazing_item')
                if (target.getAttribute('data-name') === element.getAttribute('data-name')) {
                    element.classList.remove('noactive_amazing_item')
                    element.classList.add('active_amazing_item')
                }
            })
        }
    }
    amazingtabs.addEventListener('click', selectTab)



   
    let index = 0
    $('.ham_img').click(function (e) {
        clickImgJq(e.target)
    })

    console.log($('.ham_img'))
    $('.ham_img').click(function (e) {
        Array.from($('.ham_img')).forEach(element => {
            element.classList.remove('ham_active_img')
            element.classList.remove('ham_noactive_img')
        })
        e.target.classList.add('ham_active_img')

    })

    function clickImgJq(target){
        if ($('.ham_main_photo img').attr('src') !== $(target).attr('src')) {
            console.log($(target).attr('data-name'))
            if ($(target).attr('data-name') === '2') {
                $('.ham_main_photo img').hide().attr('src', $(target).attr('src')).fadeIn(1000);
                $('.ham_main_photo span').text('Alisa')
                $('.ham_main_photo p').text('Girl')
                index = 1
            } else if ($(target).attr('data-name') === '3') {
                $('.ham_main_photo img').hide().attr('src', $(target).attr('src')).fadeIn(1000);
                $('.ham_main_photo span').text('Monkey')
                $('.ham_main_photo p').text('PHP Developer')
                index = 2
            } else if ($(target).attr('data-name') === '1') {
                $('.ham_main_photo img').hide().attr('src', $(target).attr('src')).fadeIn(1000);
                $('.ham_main_photo span').text('hasan ali')
                $('.ham_main_photo p').text('UX Designer')
                index = 0
            } else if ($(target).attr('data-name') === '4') {
                $('.ham_main_photo img').hide().attr('src', $(target).attr('src')).fadeIn(1000);
                $('.ham_main_photo span').text('Joseph')
                $('.ham_main_photo p').text('Marketolog')
                index = 3
            }
        }
    }

    let arrows = document.getElementsByClassName('arrow')
    
    let imgsArray = document.getElementsByClassName('ham_img')

    function changeIcon(event) {
        if (event.target.getAttribute('data-name') === 'right') {
            if (index < 3) {
                console.log(imgsArray[index])
                imgsArray[index].classList.remove('ham_active_img')
                imgsArray[index].classList.add('ham_noactive_img')
                index = index + 1
                imgsArray[index].classList.add('ham_active_img')
                imgsArray[index].classList.remove('ham_noactive_img')
                clickImgJq(imgsArray[index])
            } else if (index === 3) {
                imgsArray[index].classList.remove('ham_active_img')
                imgsArray[index].classList.add('ham_noactive_img')
                index = 0
                imgsArray[index].classList.add('ham_active_img')
                imgsArray[index].classList.remove('ham_noactive_img')
                clickImgJq(imgsArray[index])

            }
        } else if(event.target.getAttribute('data-name') === 'left') {
            if (index === 0) {
               
                imgsArray[index].classList.remove('ham_active_img')
                imgsArray[index].classList.add('ham_noactive_img')
                index = 3
                imgsArray[index].classList.add('ham_active_img')
                imgsArray[index].classList.remove('ham_noactive_img')
                clickImgJq(imgsArray[index])
            } else if (index < 4 ) {
                imgsArray[index].classList.remove('ham_active_img')
                imgsArray[index].classList.add('ham_noactive_img')
                index = index - 1
                imgsArray[index].classList.add('ham_active_img')
                imgsArray[index].classList.remove('ham_noactive_img')
                clickImgJq(imgsArray[index])

            }
           
        }
    }


    Array.from(arrows).forEach(element => {
        element.addEventListener('click', changeIcon)
    });

})