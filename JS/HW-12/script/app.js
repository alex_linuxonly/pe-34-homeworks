document.addEventListener('DOMContentLoaded', function () {
    let images = ['./img/2.jpg', './img/3.jpg', './img/4.png', './img/1.jpg'];
    let stopbtn = document.querySelector('#stopbtn')
    let contbtn = document.querySelector('#continue')
    let index = 0;
    const imgElement = document.querySelector('#mainPhoto');
    let timer;

    function change() {
        imgElement.src = images[index];
        if (index !== 3) {
            index++
        } else {
            index = 0;
        }

    }
    
    function Start() {
        timer = setInterval(change, 3000);
    }

    function Stop() {
        clearTimeout(timer)
    }


    stopbtn.addEventListener('click', Stop)
    contbtn.addEventListener('click', Start)

    window.onload = Start()
})