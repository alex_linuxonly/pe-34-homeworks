const themeSwitcher = document.getElementById("theme-switch");

themeSwitcher.checked = false;
let cards = document.getElementsByClassName('card_name')
let arraycards = Array.from(cards)
console.log(cards)
let whitearray = Array.from(document.getElementsByClassName('white_theme'))
console.log()
function clickHandler() {
    console.log(this)
    if (this.checked) {
        document.body.classList.remove("light");
        document.body.classList.add("dark");

        whiteThemeremove()
        localStorage.setItem("theme", "dark");

        
    } else {
        document.body.classList.add("light");
        document.body.classList.remove("dark");

        whiteThemeadd()
        localStorage.setItem("theme", "light");

        
    }
}
themeSwitcher.addEventListener("click", clickHandler);

window.onload = checkTheme();
function whiteThemeadd(){
    
    whitearray.forEach(element => {
        element.classList.add('white_themes')
    });
    arraycards.forEach(element => {
        element.classList.add('light_topics')
    });

    
}
function whiteThemeremove(){
    whitearray.forEach(element => {
        element.classList.remove('white_themes')
    });
    arraycards.forEach(element => {
        element.classList.remove('light_topics')
    });
}
function checkTheme() {
    const localStorageTheme = localStorage.getItem("theme");

    if (localStorageTheme !== null && localStorageTheme === "dark") {
        // set the theme of body
        document.body.className = localStorageTheme;

        // adjust the slider position
        const themeSwitch = document.getElementById("theme-switch");
        themeSwitch.checked =   true;
    }else{
        whiteThemeadd()
    }
}
